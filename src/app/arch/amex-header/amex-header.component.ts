import { Component, OnInit } from '@angular/core';
import { AppService} from '../../app.service';
import { Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-amex-header',
  templateUrl: './amex-header.component.html',
  styleUrls: ['./amex-header.component.less']
})
export class AmexHeaderComponent implements OnInit {
  username : string = "Amex User";
  isIntAdmin = false;
  isExtAdmin = false;
  isEAuth = false;
  isPlatformAdmin = false;
  
  guid = "b66762c1f8c6481aa4a2a533f7c77e92";  //Admin
  //guid = "0e90dc4d65264b55a05badaa8deee37a"; //Org-Admin
  //guid = "857417578666496ba260d421673da1df"; //Normal User
 // guid = "4c6676a25a0b4355b5f088e24101798b";  //E-Auth Admin

  roleObj :JSON;
  constructor(private _appService : AppService) { }

  ngOnInit(){
        console.log("inside app comp");
        this._appService.getAppRole(this.guid).subscribe(
            roleItem => {
                this.roleObj  = roleItem;
                console.log(this.roleObj);
                this.updateHeader();
            },
            error =>  {console.log(error)},
        )
     }

     updateHeader(){

         if(this.roleObj['userType'] === "Application Owner")
         {
             this.isIntAdmin = true;
             this.isExtAdmin = true;
             this.isEAuth = true;
             this.isPlatformAdmin = true;

         }else if(this.roleObj['userTypeOrg'] === "Org Admin")
         {
             /*this.isIntAdmin = false;
             this.isExtAdmin = false;
             this.isEAuth = false;*/
             this.isPlatformAdmin = true;

         }else if(this.roleObj['userRole'] === "EAuth"){
            this.isEAuth = true;
         }else{
            // Normal User
         }
     }

}
