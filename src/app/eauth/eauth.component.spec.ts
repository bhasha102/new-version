/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EauthComponent } from './eauth.component';

describe('EauthComponent', () => {
  let component: EauthComponent;
  let fixture: ComponentFixture<EauthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EauthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EauthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
