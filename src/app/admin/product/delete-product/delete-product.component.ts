import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { DeleteProductService } from './delete-product.service';
import { ProdList} from './delete-product';
@Component({
  
    selector: 'deleteproduct',
    templateUrl: './delete-product.component.html',
    styleUrls: ['./delete-product.component.less']
})
export class DeleteProductComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
     errorMessage:string;
     product:ProdList[];
     updatedMsg : boolean = false;
     errorMsg : boolean = false;
     deleteProduct : string = "0";
     response :any ;
     errorString : string = "Please select a Product.";
     constructor(private _fb: FormBuilder, private _delproductService : DeleteProductService) { }
     ngOnInit() {
        this.myForm = this._fb.group({
             deleteProduct: ['']
               });
      this.getProductsList();
     }

   getProductsList()
   {
        this._delproductService.getProducts().subscribe(
                    productDetails => this.product = productDetails,
                    error =>  {console.log(error)});
         console.log(this.product);
    }
    onChange(){
        console.log(this.updatedMsg);
        if (this.deleteProduct != undefined || this.deleteProduct ==="0")
        { 
            this.updatedMsg = false;
            this.errorMsg = false;
        }
    }

    deleteRowForm(){
       
          if (this.deleteProduct === undefined || this.deleteProduct ==="0" )
          { 
            this.errorString = "Please select a Product.";
            this.errorMsg = true;
            this.updatedMsg = false;
         }
         else
         { 
             let productID = this.deleteProduct['product_ID'];
            console.log(this.deleteProduct);
            console.log(this.deleteProduct['product_ID']);
           this._delproductService.deleteProduct(productID,this.deleteProduct['product_name']).subscribe(
                adminPl => {
                    this.response  = adminPl;
                    console.log(this.response);
                    this.updatedMsg = true;
                     if(this.response['result'] == "Success")
                    {
                            
                            this.updatedMsg = true;
                            console.log("success"+ this.updatedMsg);
                            this.errorMsg = false;
                            this.product = this.product.filter(function(qt){
                               // console.log(productID);
                               // console.log(qt.product_ID);
                                if (qt.product_ID !== productID)
                                return qt;
                            })
                        this.deleteProduct = "0";
                    }
                    else{
                        console.log("false"+ this.updatedMsg);
                        this.errorString = "Error in deleting the product.Please try after sometime.";
                        this.updatedMsg = false;
                        this.errorMsg = true;
                        
                    }
                    
                },
                error =>  {console.log(error)}
           )
                

           //console.log(deleteProduct);
           
        }
    }
}