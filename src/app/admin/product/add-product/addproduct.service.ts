import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AdminAddProductService{
private baseUrl2: string = "./app/admin/product/add-product/admin-product-apigee.json";
private baseUrl1: string = "http://localhost:8085/dev-portal-intra/addProdDetails";//"./app/admin/product/add-product/admin-product-apigee.json";
responseObj :any;
headers = new Headers();
constructor(private http : Http){
  }
     getApigeeList():Observable<Object[]>{
    return this.http.get(this.baseUrl2)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }


  // submitproduct(formData : FormData):Observable<any>{
  //     let headers = new Headers({ 'Content-Type': 'multipart/form-data','enctype': 'multipart/form-data', 'Access-Control-Allow-Origin': '*','Access-Control-Request-Headers':'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With,Access-Control-Allow-Origin','Access-Control-Request-Method': 'POST, GET, OPTIONS, DELETE'});//... Set content type to JSON
  //       let options = new RequestOptions({ headers: headers }); // Create a request
  //       return this.http.post(this.baseUrl1 , options) // ...using post request
  //                        .map((res:Response) => res.json())
  //                        .do(data => {
  //                          this.responseObj = data;
  //                        }) // ...and calling .json() on the response to return data
  //                       .catch((error:any) => Observable.throw(error || 'Server error')); //...errors if any
  // }

  submitproduct(formData: FormData): Observable<String> {
    this.headers.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    this.headers.append('Access-Control-Request-Method', 'POST, GET, OPTIONS, DELETE');
    this.headers.append('Access-Control-Allow-Origin', '*');
    //let jsonData = JSON.stringify(formData);
    return this.http.post(this.baseUrl1, formData, { headers: this.headers }) // ...using post request
      .map((res: Response) => res)
      .do(data => {
        this.responseObj = data;
      }) // ...and calling .json() on the response to return data

      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any
  }

 
      
}