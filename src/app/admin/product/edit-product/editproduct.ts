export interface editProduct{
       prodName:string,
	pvtCtgyId:string,                
	prodId:string,
	prodLocTx:string,
	prodType:string,
	apProdList:Array<string>,
	orgId:number,
	prodDs:string
}