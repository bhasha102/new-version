import { Injectable } from '@angular/core';
import {AdminProduct} from './adminproduct';
import {AdminProductSelect} from './adminproductselect';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AdminEditProductService{
//private baseUrl: string = "./app/admin/product/edit-product/adminproduct.json";
private baseUrl: string = "http://localhost:8085/dev-portal-intra/getProdList";
private baseUrl3 :string  ="http://localhost:8085/dev-portal-intra/editProdDetails";
private baseUrl1: string = "http://localhost:8085/dev-portal-intra/getProdDetail"; //./app/admin/product/edit-product/adminproductselect.json";
headers = new Headers();

  constructor(private http : Http){
  }

  getAdminProductDetails(prodId : string):Observable<AdminProduct>{
     this.headers.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
     this.headers.append('Access-Control-Request-Method', 'POST, GET, OPTIONS, DELETE');
     this.headers.append('Access-Control-Allow-Origin', '*');
       let params = new URLSearchParams();
       params.set('prodId',prodId);
      return this.http.post(this.baseUrl1,params,{headers : this.headers})
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }
   getAdminProductList():Observable<AdminProductSelect[]>{
      this.headers.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
      this.headers.append('Access-Control-Request-Method', 'POST, GET, OPTIONS, DELETE');
      this.headers.append('Access-Control-Allow-Origin', '*');
      return this.http.get(this.baseUrl,{headers : this.headers})
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }


  // Add a new product
    // getProductDetails (body: FormData): Observable<any[]> {
    //     let bodyString = body; // Stringify payload
    //     let headers      = new Headers({ 'Content-Type': 'multipart/form-data' }); // ... Set content type to JSON
    //     let options       = new RequestOptions({ headers: headers }); // Create a request option

    //     return this.http.post(this.baseUrl, body, options) // ...using post request
    //                      .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
    //                      .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    // }

    saveEditProductDetails(formData: FormData): Observable<String> {
    this.headers.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    this.headers.append('Access-Control-Request-Method', 'POST, GET, OPTIONS, DELETE');
    this.headers.append('Access-Control-Allow-Origin', '*');
    //let jsonData = JSON.stringify(formData);
    return this.http.post(this.baseUrl3, formData, { headers: this.headers }) // ...using post request
      .map((res: Response) => res)
      //.do(data => {
        //this.responseObj = data;
     // }) // ...and calling .json() on the response to return data

      .catch((error: any) => Observable.throw(error || 'Server error')); //...errors if any
  }
   
}