export interface AdminProduct{
        prodName : string,
        pvtCtgyId:string,
        prodId:string,
        prodLocTx:string,
        prodType:string,
        apProdList:[string],
        orgId:string,
        prodDs:string
}