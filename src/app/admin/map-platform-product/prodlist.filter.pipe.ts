import { Pipe, PipeTransform } from '@angular/core';
import { MapPlatformProduct} from './mapplatformproduct';

@Pipe({
    name: 'prodfilter',
    pure: false
})
export class ProdListFilterPipe implements PipeTransform {

    transform(item: MapPlatformProduct[], nameFilter: string):any {
        console.log("inside filter",item );
         if (nameFilter === undefined ) 
         {
               console.log("nameFilter : "+ nameFilter);
               return item;
         }
          console.log("searched text", nameFilter) 
          return item.filter(function(qt){
              if (qt.product_name.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.product_name.toLowerCase().includes(nameFilter.toLowerCase());
          })
    }
}