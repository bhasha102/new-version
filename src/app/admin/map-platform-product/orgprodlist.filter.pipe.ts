import { Pipe, PipeTransform } from '@angular/core';
import { MapPlatformProduct} from './mapplatformproduct';

@Pipe({
    name: 'orgfilter',
    pure: false
})
export class OrgProdListFilterPipe implements PipeTransform {

    transform(item: MapPlatformProduct[], productnameFilter: string):any {
        console.log("inside filter",item );
         if (productnameFilter === undefined ) 
         {
               console.log("nameFilter : "+ productnameFilter);
               return item;
         }
          console.log("searched text", productnameFilter) 
          return item.filter(function(qt){
              if (qt.product_name.toLowerCase().indexOf(productnameFilter.toLowerCase()) !== -1)
              return qt.product_name.toLowerCase().includes(productnameFilter.toLowerCase());
          })
    }
}