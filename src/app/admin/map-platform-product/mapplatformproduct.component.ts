import { Component, EventEmitter, Input, OnChanges, OnInit,ViewChildren } from '@angular/core';
import { ManageUserService} from '../platform/manageuser.service';
import { MapPlatformProductService} from './mapplatformproduct.service';
import { Platform} from '../platform/platform';
import { Observable} from 'rxjs/Rx';
import { MapPlatformProduct} from './mapplatformproduct';


@Component({
    selector: 'map-platform-product',
    templateUrl: './mapplatformproduct.component.html',
    styleUrls : ['./mapplatformproduct.component.less']
})
export class MapPlatformProductComponent implements OnInit {
        @ViewChildren('list1') selectElRef;
        @ViewChildren('list2') selectPlRef;
        @ViewChildren('list3') deletePlRef;
        platformDetails:Platform[];
        errorMessage:string;
        sortVal : string;
        sortType : string = 'Asc';
        organizationId: string;
        responseObj :JSON;
        masterProdList: MapPlatformProduct[];
        prodList: MapPlatformProduct[] = [];
        orgProdList: MapPlatformProduct[] = [];
        prodListOrg: MapPlatformProduct[] =[];
        Indicator:string;
        showModal:boolean = false;
        pname :string;
        selectedOrgList:Array<string> = [''];
        selectedProdList:Array<string> = [''];
        List: any =[];
        List2: any =[];
        List3: any =[];
        isSelected : boolean = false;
        nameFilter : string;
        productnameFilter : string;
        textprodlist : string;
        textorglist  : string;
        showorgtext  : boolean = false;
        showorgfilter: boolean = false;
        showprodtext  : boolean = false;
        showprodfilter: boolean = false;
        deletedList:Array<string> = [''];
        deleteMapList:MapPlatformProduct[] = [];
        constructor( private _platformProductService : MapPlatformProductService, private _adminService: ManageUserService ) {           
        }
       

    ngOnInit(){
        this.getPlatformList();
           }

    getPlatformList()
     {
        this._adminService.getPlatforms().subscribe(
                        platform => this.platformDetails = platform,
                        error =>  {console.log(error)});
                        
     }
      closeModal(event){
        if(event.target.className == "modal"){
            this.showModal =false;
             this.prodList = [];
             this.orgProdList = [];
             this.selectedOrgList = [];
             this.selectedProdList = [];
             this.List = [];
             this.List2 = [];
             this.isSelected = false;
             this.List3 = [];
        }
    }

     close()
    {
         this.showModal =false; 
         this.prodList = [];
         this.orgProdList = [];
         this.selectedOrgList = [];
         this.selectedProdList = [];
         this.List = [];
         this.List2 = [];
         this.isSelected = false;
         this.List3 = [];
    }

     text()
     {
         this.showorgtext = true;
         this.showprodtext = true;
         this.textprodlist = this.prodList.length > 0 ? 'showing all '+this.prodList.length : 'Empty';
         this.textorglist  = this.orgProdList.length > 0 ? 'showing all '+this.orgProdList.length : 'Empty';
         console.log("text prodlist",  this.textprodlist );
          console.log("text orgprodlist",  this.textorglist );
     }
     getMapPlatformProductDetails(){
        this._platformProductService.getMapProductDetails(this.organizationId).subscribe(
            response => {
                    this.responseObj = response;
                    this.prodListOrg = this.responseObj[0]['prodListforOrg'];
                    this.masterProdList = this.responseObj[0]['prodList'];
                    this.prodList = this.masterProdList;
                    this.text();
                    },
            error => {console.log(error)},
        );
     }
     
      addMapPlatformProductDetails(){
        this._platformProductService.addMapProductDetails(this.organizationId,this.Indicator).subscribe(
            response => {
                    this.responseObj = response;
                    this.prodListOrg = this.responseObj['prodListforOrg'];
                    this.masterProdList = this.responseObj['prodList'];
                    },
            error => {console.log(error)},
        );
     }

     productDetail(isVisible,item)
     {
        this.showModal = isVisible;
        this.organizationId = item.organizationId;
        this.pname = item.orgName;
        this.getMapPlatformProductDetails();
     }

     sort(type,key){
        if(type == 'Asc')
        {
            this.sortVal = key;
            this.sortType = 'Desc';
        }else if(type == 'Desc')
        {
            this.sortVal = "-"+key;
            this.sortType = 'Asc';
        }
    }
     
     mapOrganisationToProducts()
     {
         this.orgProdList.forEach((a) => {  if(this.prodListOrg.indexOf(a) == -1) this.prodListOrg.push(a)});
          this.orgProdList.forEach((a) => 
                        this.masterProdList = this.masterProdList.filter(ft =>
                        { 
                            if(ft.product_ID !== a.product_ID)
                                return true;
                        }))
         this.orgProdList = [];
         this.text();
         this.selectedOrgList = [];
         this.selectedProdList = [];
         this.List = [];
         this.List2 = [];
         this.isSelected = false;
         this.List3 = [];
         console.log("Maaped Product list",this.prodListOrg)
         console.log("Current ProdList" ,this.prodList)

     }
     deleteOrganisationToProducts()
     {

            this.deletePlRef.toArray().find((e) => {
                    console.log("selected ProdList", e.nativeElement.options)
                    this.deletedList = Array.apply(null,e.nativeElement.options)
                    .filter(option => option.selected)
                    .map(option => option.value)
                    })
                    let temp = this.deletedList;

                       this.deleteMapList = this.prodListOrg.filter(function(qt){
                               for(let i=0; i < temp.length; i++) 
                               {  
                                  if (qt.product_ID == temp[i])
                                  return qt;
                               }
                        });
                      this.deleteMapList.forEach((a) => {  if(this.masterProdList.indexOf(a) == -1) this.masterProdList.push(a)}) 
                       this.deleteMapList.forEach((a) => {  if(this.prodList.indexOf(a) == -1) this.prodList.push(a)}) 
                         this.deleteMapList.forEach((a) => 
                        this.prodListOrg = this.prodListOrg.filter(ft =>
                        { 
                            if(ft.product_ID !== a.product_ID)
                                return true;
                        }))
                                
        // this.orgProdList.forEach((a) => {  if(this.prodListOrg.indexOf(a) == -1) this.prodListOrg.push(a)});
        //   this.orgProdList.forEach((a) => 
        //                 this.masterProdList = this.masterProdList.filter(ft =>
        //                 { 
        //                     if(ft.product_ID !== a.product_ID)
        //                         return true;
        //                 })) 
     }

     move(item,direction)
     {
         //var selectedItems = this.prodList.filter((item) => { return item.selected });
         //console.log('selectedItems');
         //console.log(selectedItems);

         
         if(direction == 'right')
         {
            
               if(item == 'all' )
               { 
                  this.prodList = [];
                  this.List3 = [];
                 if (this.nameFilter === undefined ) 
                 { 
                      console.log( "right all component if 1 ", this.nameFilter)
                      this.orgProdList = this.masterProdList;
                 }
                  else
                  { 
                      console.log( "right all component if 1 ", this.nameFilter)
                   //this.orgProdList = [];
                   this.masterProdList.forEach((a) => { if(a.product_name.toLowerCase().indexOf(this.nameFilter.toLowerCase()) !== -1 && this.orgProdList.indexOf(a) == -1) this.orgProdList.push(a)})    
                    
                    console.log("right all Entire orgList", this.orgProdList)
                      this.prodList = this.masterProdList;
                        this.orgProdList.forEach((a) => 
                        this.prodList = this.prodList.filter(ft =>
                        { 
                            if(ft.product_ID !== a.product_ID)
                                return true;
                        }))
                         this.List3 = this.prodList;
                  }


                  // this.orgProdList = this.masterProdList;
                   
                   this.selectedOrgList = [];
                   this.selectedProdList = [];
                   this.List = [];
                   this.List2 = [];
                   this.isSelected = false;
                   console.log("right all orgProdList" ,this.orgProdList)
                   
               }

                if(item == 'selected')
                { 
                      this.selectElRef.toArray().find((e) => {
                    console.log("selected ProdList", e.nativeElement.options)
                     this.change(e.nativeElement.options, "right")
                    })
                }
                    console.log("right is selected before if", this.isSelected )
               if(item == 'selected' && this.isSelected)
               {
                    
                     this.prodList = this.masterProdList;
                    console.log("right selectedProdList" ,this.selectedProdList)
                       this.orgProdList = this.masterProdList;
                       let temp = this.selectedProdList;
                       this.orgProdList = this.orgProdList.filter(function(qt){
                               for(let i=0; i < temp.length; i++) 
                               {  
                                  if (qt.product_ID == temp[i])
                                  return qt;
                               }
                        });
                                   console.log("right OrgList and tempList" ,this.orgProdList, this.List )
                        this.orgProdList.forEach((a) => {  if(this.List.indexOf(a) == -1) this.List.push(a)})                         
                              this.orgProdList = this.List;
                              console.log("right after push OrgList and tempList " ,this.orgProdList, this.List )
                        this.orgProdList.forEach((a) => 
                        this.prodList = this.prodList.filter(ft =>
                        { 
                            if(ft.product_ID !== a.product_ID)
                                return true;
                        }))
                        this.List3 =  this.prodList;
                        console.log("right deleted filtered prodList " ,this.prodList )
                            console.log("temp List3" ,this.List3 )
                            this.List2 = [];
                           this.selectedProdList = [];
                           this.selectedOrgList = [];
                           this.isSelected = false;
                            console.log("right is selected within if", this.isSelected )
                   }
                       
            }
       

         else
         {
            
               if(item == 'all' )
               { 
                     this.orgProdList = [];
                    if (this.productnameFilter === undefined ) 
                     { 
                          console.log( "left all component if 1 ", this.productnameFilter)
                          this.prodList = this.masterProdList;
                     }
                  else
                  { 
                      console.log( "left all component if 1 ", this.productnameFilter)
                   // this.prodList = [];
                   this.masterProdList.forEach((a) => { if(a.product_name.toLowerCase().indexOf(this.productnameFilter.toLowerCase()) !== -1  && this.prodList.indexOf(a) == -1) this.prodList.push(a)})    
                    
                    console.log("left all Entire prodList", this.prodList)
                     this.orgProdList = this.masterProdList;
                        this.prodList.forEach((a) => 
                        this.orgProdList = this.orgProdList.filter(ft =>
                        { 
                            if(ft.product_ID !== a.product_ID)
                                return true;
                        }))

                  }

                   //this.prodList = this.masterProdList;
                   
                   this.selectedOrgList = [];
                   this.selectedProdList = [];
                   this.List = [];
                   this.List3 = [];
                   this.List2 = [];
                   this.isSelected = false;
                      console.log("left all ProdList" ,this.prodList)
               }
                if(item == 'selected')
                { 
                    this.selectPlRef.toArray().find((e) => {
                    console.log("selected orgList", e.nativeElement.options)
                     this.change(e.nativeElement.options, "left")
                    })
                }
                    console.log("left is selected before if", this.isSelected )
               if(item == 'selected' && this.isSelected)
               {
                   

                   console.log("left selectedOrg List" ,this.selectedOrgList)
                
                       console.log("temp List3" ,this.List3 )
                       this.List2 =  this.List3;
                        this.prodList = this.masterProdList;
                        console.log("list2 initial" ,this.List2 )
                   let temp = this.selectedOrgList;
                       this.prodList = this.prodList.filter(function(qt){
                               for(let i=0; i < temp.length; i++) 
                               {  
                                  if (qt.product_ID == temp[i])
                                  return qt;
                               }
                        });
                        console.log("left prodList and temp list" ,this.prodList, this.List2 )
                
                        this.prodList.forEach((a) => { if(this.List2.indexOf(a) == -1) this.List2.push(a)})                         
                              this.prodList = this.List2;
                               console.log("left after push prodList and temp List" ,this.prodList, this.List2 )
                        this.prodList.forEach((a) => 
                        this.orgProdList = this.orgProdList.filter(ft =>
                        { 
                            if(ft.product_ID !== a.product_ID)
                                return true;
                        }))
                        this.List = [];
                        this.orgProdList.forEach((a) => {  if(this.List.indexOf(a) == -1) this.List.push(a)})            
                        this.selectedProdList = [];
                        this.selectedOrgList = [];
                        this.isSelected = false;
                         console.log("left is selected within if", this.isSelected )

                      console.log("left deleted filtered OrgprodList" ,this.orgProdList )
                   }
               }
               this.text();
         }
    

 change(options,direction) {
     console.log("options",options , options.selectedIndex);
      console.log("option length",options.length);
      console.log("option is selected", this.isSelected)
      if(options.length > 0)
         this.isSelected = true;
     if(direction == 'right')
     { 
      this.selectedOrgList = []
      this.selectedProdList = Array.apply(null,options)
      .filter(option => option.selected)
      .map(option => option.value)

     //     this.selected(options,this.selectedProdList)
   
     }
     else
     {
          this.selectedProdList = []
          this.selectedOrgList = Array.apply(null,options)
      .filter(option => option.selected)
      .map(option => option.value)
          
      //    this.selected(options,this.selectedOrgList)
     }
     console.log("selectedProdList final",this.selectedProdList);
       console.log("selectedOrgList Final",this.selectedOrgList);
      
  }

  resetFilter()
  {
      this.nameFilter = "";
  }

//   getEvent(event){
//     let options = this.selectElRef[0];
//     this.selectElRef.toArray().find((e) => {
//      console.log( e.nativeElement.options)
//      this.change(e.nativeElement.options, "right")
//     })
  //}
//   selected(options,selectedValue)
//   {
//          for(let i=0; i < options.length; i++) {
//              options[i].selected = selectedValue.indexOf(options[i].value) > -1;
//     }
//  }
    
}