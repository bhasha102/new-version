import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { MapPlatformProduct} from './mapplatformproduct';
import { JsonpModule} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MapPlatformProductService{
  private baseUrl: string = "./app/admin/map-platform-product/mapplatformproduct.json";
  public responseObj:JSON;
  constructor(private http : Http){
       this.responseObj = null;
  }

 /* getMapProductDetails (organizationId:string): Observable<JSON> {
        let bodyString = JSON.stringify({prodID: organizationId}); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*','Access-Control-Request-Headers':'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin','Access-Control-Request-Method': 'POST, GET, OPTIONS, DELETE'});//... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        console.log(bodyString);
         return this.http.post(this.baseUrl, bodyString, options) // ...using post request
                         .map((res:Response) => res.json())
                         .do(data => {
                           this.responseObj = data;
                         }) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error || 'Server error')); //...errors if any
                         
    } */

    /* addMapProductDetails (organizationId:string, Indicator:string): Observable<JSON> {
        let bodyString = JSON.stringify({prodID: organizationId,indicator:Indicator}); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*','Access-Control-Request-Headers':'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Access-Control-Allow-Origin','Access-Control-Request-Method': 'POST, GET, OPTIONS, DELETE'});//... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        console.log(bodyString);
         return this.http.post(this.baseUrl, bodyString, options) // ...using post request
                         .map((res:Response) => res.json())
                         .do(data => {
                           this.responseObj = data;
                         }) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error || 'Server error')); //...errors if any
                         
    }*/

 getMapProductDetails (organizationId:string): Observable<JSON> {
const headers = new Headers();
	headers.append('Access-Control-Allow-Headers', 'Content-Type');
	headers.append('Access-Control-Allow-Methods', 'GET');
	headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
 }

      addMapProductDetails (organizationId:string, Indicator:string): Observable<JSON> {
          const headers = new Headers();
	headers.append('Access-Control-Allow-Headers', 'Content-Type');
	headers.append('Access-Control-Allow-Methods', 'GET');
	headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
 }
}