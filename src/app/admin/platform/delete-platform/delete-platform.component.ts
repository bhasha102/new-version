import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { DeletePlatformService } from './delete-platform.service';
import { ManageUserService} from '../manageuser.service';
import { AddPlatformService } from '../add-platform/addplatform.service';
import { Platform} from '../platform';
@Component({
  
    selector: 'deleteplatform',
    templateUrl: './delete-platform.component.html',
    styleUrls: ['./delete-platform.component.less']
})
export class DeletePlatformComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
     errorMessage:string;
     platform : Platform[];
     updatedMsg : boolean = false;
     deletePlatform : string = "0";
     errorMsg : boolean = false;
     returnVal : JSON;
     errorString : string = "Please select a Platform.";
     constructor(private _fb: FormBuilder, private _delplatformService : DeletePlatformService, private _addplatformService : AddPlatformService, private _manageUserService : ManageUserService) { }
     ngOnInit() {
        this.myForm = this._fb.group({
             deleteplatform: ['']
               });
      this.getPlatformsList();
     }

    getPlatformsList()
    {
        this._manageUserService.getPlatforms().subscribe(
                        platformDetails => this.platform = platformDetails,
                        error =>  {console.log(error)});
        console.log(this.platform);
    }

   onChange(){
        if (this.deletePlatform != undefined || this.deletePlatform ==="0")
        { 
        console.log(this.updatedMsg);
        this.updatedMsg = false;
        this.errorMsg = false;
        }
    }

    deleteRowForm(){
        console.log(this.deletePlatform);
        if (this.deletePlatform == undefined || this.deletePlatform === "0" )
          { 
            this.errorString = "Please select a Platform.";
            this.errorMsg = true;
            this.updatedMsg = false;
        }
        else
            {
                let action = "D";
                let orgObj = {"orgId":+this.deletePlatform};
                let toRemovePlatform = this.deletePlatform;
                this._addplatformService.submitPlatform(orgObj,action).subscribe(
                adminPl =>{ 
                this.returnVal = adminPl;
                console.log(this.returnVal);
                if(this.returnVal['successFlag'])
                {
                    console.log("Success");
                    //this.isSuccess = true;
                    this.updatedMsg = true;
                    this.myForm.reset();
                    this.submitted = true;
                    this.updatedMsg = true;
                    this.errorMsg = false;
                    this.platform = this.platform.filter(function(qt){
                        if (qt.organizationId !== +toRemovePlatform)
                        return qt;
                    })
                    this.deletePlatform = "0";
                }
                else{
                    this.errorString = "Error.Please try after some time.";
                    console.log("Failure");
                    this.errorMsg = true;
                    this.updatedMsg = false;
                   // this.isFailure = true;
                }
            },
                error => { console.log(error) },
                () => { console.log(this.returnVal) }
                )

        }
    }
}