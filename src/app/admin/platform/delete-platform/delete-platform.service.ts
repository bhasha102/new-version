import { Injectable } from '@angular/core';
import { PList} from './delete-platform';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DeletePlatformService{
private baseUrl: string ="./app/admin/platform/delete-platform/delete-platform.json";
constructor(private http : Http){
  }
  
  getPlatforms():Observable<PList[]>{
       
    return this.http.get(this.baseUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
     
  }
    
}