import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { Platform } from '../platform';
import { EditPlatformService } from './editplatform.service';
import { AddPlatformService } from '../add-platform/addplatform.service';
import { ManageUserService } from '../manageuser.service';
import { Country } from '../country';
import { States } from '../states';


@Component({
  
    selector: 'editPlatform',
    templateUrl: './editplatform.component.html',
    styleUrls: ['./editplatform.component.less']
})
export class EditPlatformComponent implements OnInit {
    public myForm: FormGroup; // our model driven form
    public submitted: boolean; // keep track on whether form is submitted
    public events: any[] = []; // use later to display form changes
    country : Array<Country>;
    states :  Array<States>;
    errorMessage:string;
    platformList : Platform[];
    platformDetails : Platform[];
    success: string;
    error : string;
    showMessage = false;
    submitMessage :string;
    successMsg = false;
    selectCountry : string = 'US';
    selectStates :string = '001';
    updatedMsg :boolean = false;
    returnVal :JSON;
    selectedPlatform: string = "0";
    constructor(private _fb: FormBuilder, private _editplatformService : EditPlatformService, private _platformListService : ManageUserService, private _addplatformService :AddPlatformService) { } // form builder simplify form initialization

    ngOnInit() {
        this.myForm = this._fb.group({
            orgName : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            platformName : [''],
            phoneNumber : ['', [<any>Validators.required]],
            addressLine1: ['', [<any>Validators.required,Validators.pattern("[a-zA-Z0-9][a-zA-Z0-9 \-]*")]],
            addressLine2: [''],
            city : ['', [<any>Validators.required,Validators.pattern("[a-zA-Z][a-zA-Z0-9 \-]*")]],
            zip : ['', [<any>Validators.required,Validators.minLength(5),Validators.maxLength(5)]],
            country : [''],
            states: ['']
        });

        this.getPlatformList();
        this.getCountryList();
        this.getStatesList("US");
       
     
    }
    
    removeUpdateMsg()
        {
            this.updatedMsg = false;
                      //this.successMsg = false;
        }
  
    getPlatformList()
     {
        this._platformListService.getPlatforms().subscribe(
                        platformDetails => this.platformList = platformDetails,
                        error =>  {console.log(error)});
                        
     }
     onChange(orgId)
     {
            // Need to pass orgId in service
            if (orgId =="0")
            {
                console.log(orgId)
                  this.myForm.reset();
                  this.selectedPlatform = "0";
            }
            else
            { 
                console.log("get details")
                console.log(orgId)
            this._editplatformService.getPlatformDetails(orgId).subscribe(
                        UserDetails => this.platformDetails = UserDetails,
                        error =>  {console.log(error)},
                        () => {this.updatePlatformDetails()}         
            );
            this.showMessage =false;
            }
     //   this._addplatformService.getUserDetails(plat).subscribe(
    //        response => this.responseObj = response,
     //       error => {console.log(error)}
     //   );
     }

     updatePlatformDetails()
     {
         //this.myForm.patchValue({'city':this.platformDetails[0].city},{'orgName': this.platformDetails[0].orgName});
         this.myForm.controls['orgName'].patchValue(this.platformDetails[0].orgName);
         this.myForm.controls['phoneNumber'].patchValue(this.platformDetails[0].phoneNumber);
         this.myForm.controls['addressLine1'].patchValue(this.platformDetails[0].addressLine1);
         this.myForm.controls['addressLine2'].patchValue(this.platformDetails[0].addressLine2);
         this.myForm.controls['zip'].patchValue(this.platformDetails[0].zip.toString().trim());
         this.myForm.controls['city'].patchValue(this.platformDetails[0].city);
         this.getStatesList(this.platformDetails[0].country); //country = countryCode
         this.selectCountry = this.platformDetails[0].country;
         
     }

    getCountryList()
    {
        this._addplatformService.getCountryList().subscribe(
             country => this.country  = country,
             error =>  {console.log(error)});
    }
    getStatesList(countryCode)
    {
        // Pass country code to service
        this._addplatformService.getStatesList(countryCode).subscribe(
             states => this.states  = states,
             error =>  {console.log(error)},
             () => {

                 this.selectStates = this.platformDetails != undefined ? this.platformDetails[0].state : '001'}
             );
    }

    save(model: any, isValid: boolean) {

       let input = new Object;
       input["orgName"]= model.platformName;
       input["addressLine1"]= model.addressline1;
       input["addressLine2"]= model.addressline2;
       input["country"]= model.country;
       input["state"]= model.states;
       input["phoneNumber"]= model.phoneNumber;
       input["city"]= model.city;
       input["zip"]= model.zip;
       input["orgId"]= +this.platformDetails[0].organizationId;
       
       console.log(model);

       let action = "U";

       this._addplatformService.submitPlatform(input,action).subscribe(
            adminPl =>{ 
                this.returnVal = adminPl;
                console.log(this.returnVal);
                if(this.returnVal['successFlag'])
                {
                    console.log("Success");
                    //this.isSuccess = true;
                    this.updatedMsg = true;
                    //this.myForm.reset();
                    this.submitted = true;
                }
                else{
                    //this.failureMsg = "Error.Please try after some time.";
                    console.log("Failure");
                   // this.isFailure = true;
                }
            }
            ,
            error => { console.log(error) },
            () => { console.log(this.returnVal) }
        )




        // check if model is valid
        // if valid, call API to save customer
        //call Service menthod and pass the values to it
        //pass the form Data to service
        this.submitted = true;  // set form submit to true
        // this._editplatformService.saveEditPlatform(model).subscribe(
        //     success => this.success = success,
        //     error => this.error = error,
        //     () => {
        //             console.log("done");
        //             this.showMessage =true;
        //             if(this.success == "success")
        //             {
        //                 this.submitMessage = "Platform successfully updated.";
        //                 this.successMsg = true;
        //                 this.myForm.reset();
        //                 console.log("success");
  
        //             }
        //             else{
        //                 this.submitMessage = "Error while updating platform.";
        //                 this.successMsg = false;
        //             }
                        
        //         }  
                      
        // )
        
        //  this.myForm.reset();
        console.log(model, isValid);
    }
}