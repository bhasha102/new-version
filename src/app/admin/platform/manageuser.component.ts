import { Component, OnInit } from '@angular/core';
import { Platform } from './platform';
import { ManageUserService} from './manageuser.service';
import { FormGroup, FormControl, FormBuilder, Validators  } from '@angular/forms';
import { UserList} from './user-list';
import { ChangeDetectorRef} from '@angular/core';
import { ManageUserFilterPipe} from './manageuser-filter.pipe';

@Component({
  selector: 'platform',
  templateUrl: './manageuser.component.html',
  styleUrls: ['./manageuser.component.less']
})
export class ManageUserComponent implements OnInit {
errorMessage:string;
Platform:Platform[] ;
public myForm: FormGroup; 
responseObj :JSON;
responseTxt:string;
userDetails: UserList[];
selectedState :any;
showtable = false;
showModal : boolean = false;
//radioItems = ['ADMIN','EAUTH ADMIN','MERCHANT USER','ORG ADMIN','USER'];
model = { options: 0 };
userId : string;
roles : Object[];
updatedMsg:boolean = false;
userRole :string;
sortVal : string;
sortType : string = 'Asc';
errorMsg : boolean = false;
statusIDs : Array<string> = [];
statusArray : Array<string> = [];
userRoleId : string = "0";
selectedObj : Object={};
updateResponse :any;
roleUpdatedMsg : string;
failureMsg :boolean = false;
rolefailureMsg : string;
viewModal : boolean = false;
selectedPlatform = "0";

    public constructor( private _fb: FormBuilder, private _adminService: ManageUserService, private changeDetectorRef: ChangeDetectorRef ) {
    }

  ngOnInit(){
       this.myForm = this._fb.group({
           adminPlatform : [''],
           search :['']
       }),
        this.getPlatformList();
        this.getRoles();
 }

     getPlatformList()
     {
        this._adminService.getPlatforms().subscribe(
                        platformDetails => this.Platform = platformDetails,
                        error =>  {console.log(error)}); 
     }
   getRoles()
   {
       this._adminService.getRoles().subscribe(
                        roles => this.roles = roles,
                        error =>  {console.log(error)});
   }


  platformDetail(){
        console.log("inisde ");
    }

    onChange(orgId){
        console.log("orgId", orgId)
       if(orgId == "0")
       { 
            this.errorMsg = false;
        this.showtable = false;
       }
       else
       { 
           console.log("inside get details")
        this.errorMsg = false;
        this.showtable = true;
        this._adminService.getUdetails(orgId.toString()).subscribe(
                        UserDetails =>this.userDetails = UserDetails,
                        error =>  {console.log(error)});

       }
     //   this._adminService.getUserDetails(plat).subscribe(
    //        response => this.responseObj = response,
     //       error => {console.log(error)}
     //   );
    }
     sort(type,key){
         this.errorMsg = false;
        if(type == 'Asc')
        {
            this.sortVal = key;
            this.sortType = 'Desc';
        }else if(type == 'Desc')
        {
            this.sortVal = "-"+key;
            this.sortType = 'Asc';
        }
    }

    /*deleteRowForm(){
        let j=0;
         for(let i =0;i< this.userDetails.length;i++)
         {
               if(this.userDetails[i].state)
                  j++;
         }
         if(j === 0)
         this.errorMsg = true;
         else
          this.errorMsg = false;
       
       this.userDetails = this.userDetails.filter(function(qt){
            console.log(qt.state);
              if (qt.state === false || qt.state == undefined)
                   return qt;
    })
      
}*/


changeStatusOrDelete(selectedPlatform,indicator){
         
         let j=0;
         for(let i =0;i< this.userDetails.length;i++)
         {
               if(this.userDetails[i].state)
               {
                     this.statusIDs.push(this.userDetails[i].userId.toString());
                     this.statusArray.push(this.userDetails[i].status);
                     console.log(this.statusIDs);
                     console.log(this.statusArray);
                     
                      
                    // Delete this code
                     /*if(this.userDetails[i].status === "Y"){
                         this.userDetails[i].status = "N";
                     }
                     else{
                        this.userDetails[i].status = "Y";
                    }*/

                   // this.userDetails[i].state = false;
                    j++;
               }
         }

         this._adminService.changeStatusOrDelete(selectedPlatform.toString(),this.statusIDs,this.statusArray,indicator).subscribe(
                    UserDetails => this.userDetails = UserDetails,
                    error =>  {console.log(error)},
                    () => {
                        this.statusIDs = [];
                        this.statusArray = [];
                    }
        );

         if(j === 0){
            this.errorMsg = true;
        }
        else{
            this.errorMsg = false;
         }
}

    useridactive(isvisible,item){
        this.userId = item.userId;
        this.showModal =isvisible;
        this.selectedObj = item;
        console.log(item);
       // this.model.options = item != '' || item != null ? item.roleId : '';
        this.userRoleId = item != '' || item != null ? item.roleId : '';
        console.log(this.userRoleId);
        this.updatedMsg = false;
        this.errorMsg = false;
        this.failureMsg = false;  
    }

    closeModal(event){
        //console.log(event.target.className);
        if(event.target.className == "modal"){
            this.showModal =false;
            this.viewModal =false;
        } 
    }

    updateUserRole(selectedPlatform){
        let roleName = "";
        this.roles.forEach(element => {
            if(element['roleId'] == this.userRoleId)
            {
                roleName = element['roleName'];
            }
        });
       
        if(this.selectedObj['roleId'] === this.userRoleId)
        {
            this.updatedMsg = true;
            this.failureMsg = false;
            this.roleUpdatedMsg = "Please choose a different role to update";
        }
        else{
            this._adminService.updateUserRole(selectedPlatform.toString(),this.userRoleId,roleName,this.selectedObj['guid'],this.selectedObj['userId']).subscribe(
                UserDetails =>{
                    this.updateResponse = UserDetails;
                    console.log(this.updateResponse[0]['userId']);
                    if(this.updateResponse[0]['userId'] != null)
                    { 
                        this.updatedMsg = true;
                        this.failureMsg = false;
                        this.updateResponse = UserDetails;
                        this.roleUpdatedMsg = "Roles Updated. Please close the popup and refresh the page to see the change";
                    }
                    else{
                        this.failureMsg = true;
                        this.updatedMsg = false;
                        this.rolefailureMsg = "Failed to Update Role. Kindly try after sometime.";
                    }
                },
                error =>  {console.log(error)},
                () => {
                    console.log(this.updateResponse);
                }
            );
        }
        

        // Send newuserRole to Service and update
        // this.updatedMsg = true;
    }


    adduser(show){
        console.log("add user",show)
        this.viewModal = show;
    }




}

