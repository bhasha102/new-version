
export interface Platform
{
    orgName: string,
    phoneNumber : number,
    addressLine1 : string,
    addressLine2 : string,
    country : string,
    state : string,
    city : string,
    zip :number,
    organizationId : number,
    publicGUID : string,
    dateCreated : Date,
    orgId : number
}
/*
[{
	"orgName": "bhavikk",
	"phoneNumber": "1234567890",
	"addressLine1": "jdbjdsbhbs",
	"addressLine2": "ndjsnfns",
	"city": "phoenix",
	"zip": "90211",
	"country": "US",
	"state": "002",
	"organizationId": "1043",
	"publicGUID": null,
	"appdatalist": null,
	"dateCreated": "2016-12-22 16:49:17",
	"orgId": 0
}]
*/