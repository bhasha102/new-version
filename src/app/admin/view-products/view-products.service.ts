import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ViewProducts} from './view-product';
import {JsonpModule} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ViewProductsService{
  private baseUrl: string = 'http://localhost:8085/dev-portal-intra/getOrgMappedProductsList';  //'./app/admin/view-products/ViewProducts.json';
 
  constructor(private http : Http){
  }

  getAllProducts():Observable<ViewProducts[]>{
	const headers = new Headers();
	headers.append('Access-Control-Allow-Headers', 'Content-Type');
	headers.append('Access-Control-Allow-Methods', 'GET');
	headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(this.baseUrl)
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw('Server error'))
  }
}