import { Pipe, PipeTransform } from '@angular/core';
import { ViewAllUsers} from './view-all-users';

@Pipe({
    name: 'filter',
})
export class ViewAllUsersPipe implements PipeTransform {

    transform(question: ViewAllUsers[], nameFilter: string):any {
        console.log(question );
         if (nameFilter === undefined ) 
         {
               console.log("nameFilter : "+ nameFilter);
               return question;
         }
       
          return question.filter(function(qt){
              if (qt.status.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.status.toLowerCase().includes(nameFilter.toLowerCase());
              if (qt.organization.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.organization.toLowerCase().includes(nameFilter.toLowerCase()); 
              if (qt.fName.toString().toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.fName.toString().toLowerCase().includes(nameFilter.toLowerCase()); 
               if (qt.lName.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.lName.toLowerCase().includes(nameFilter.toLowerCase());
              if (qt.emailAddress.toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.emailAddress.toLowerCase().includes(nameFilter.toLowerCase()); 
              if (qt.userId.toString().toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.userId.toString().toLowerCase().includes(nameFilter.toLowerCase());
              if (qt.roleName.toString().toLowerCase().indexOf(nameFilter.toLowerCase()) !== -1)
              return qt.roleName.toString().toLowerCase().includes(nameFilter.toLowerCase());
          })
          
    }
}