import { Component, EventEmitter, Input, OnChanges, OnInit,ViewEncapsulation, ElementRef,Output} from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { ViewAllUsersService} from './view-all-users.service';
import { Router } from '@angular/router';
import { ViewAllUsers} from './view-all-users';
import { ChangeDetectorRef} from '@angular/core';
import {Observable} from 'rxjs/Rx';
declare let jsPDF;

@Component({
        selector: 'product-container',
        templateUrl: './view-all-users.component.html',
        styleUrls : ['./view-all-users.component.less'],
        encapsulation:ViewEncapsulation.None,
    
 
})
export class ViewAllUsersComponent implements OnInit {
        pageTitle:string;
        viewAllUsers:ViewAllUsers[];
        tempAllUsers : ViewAllUsers[];
        errorMessage:string;
        isInActiveCheckbox : boolean = false;
        showModal : boolean = false;
        private timer;
        ticks=0;
        sortVal : string;
        sortType : string = 'Asc';
        errorMsg : boolean = false;

        constructor(private titleService: Title, private _viewAllUsers : ViewAllUsersService, private router:Router ) { 
            this.setTitle();
        }
        getTitle(){
            this.pageTitle = this.titleService.getTitle();
        }
        setTitle() {
            this.getTitle();       
            this.titleService.setTitle( this.pageTitle +" Product" );
    }

        ngOnInit(){
             this.getDetails();
    }

        getDetails()
    {
            this._viewAllUsers.getViewAllUsers().subscribe(
                        users => {this.viewAllUsers = users;
                        },
                        error =>  {console.log(error)},
                        () => {this.tempAllUsers = this.viewAllUsers; this.showInActiveStatus(false);}
                        );                                       
    }

   
download(){
       
      let newUser :Array<Object> =[];
        this.viewAllUsers.forEach(element => {
            let tempObj = {};
        if(element['status']=='Y'){
                tempObj['STATUS']='ACTIVE';
            }else{
                tempObj['Status']='INACTIVE';
            }
            tempObj['PLATFORM'] = element['organization'];
            tempObj['FIRST NAME'] = element['fName'];
            tempObj['LAST NAME'] = element['lName'];
            tempObj['EMAIL ID'] = element['emailAddress'];
            tempObj['USER ID'] = element['userId'];
            tempObj['ROLE'] = element['roleName'];
            newUser.push(tempObj);
        });

        var csvData = this.ConvertToCSV(newUser);
        var a = document.createElement("a");
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        var blob = new Blob([csvData], { type: 'text/csv' });
        var url= window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'Api-dev-portal.csv';
        a.click();
   
    } 

ConvertToCSV(objArray):string {
            var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
            var str = '';
            var row = "";
 
 
            for (var index in objArray[0]) {
                //Now convert each value to string and comma-separated
                row += index + ',';
            }
            row = row.slice(0, -1);
            //append Label row with line break
            str += row + '\r\n';
 
            for (var i = 0; i < array.length; i++) {
                var line = '';
                for (var index in array[i]) {
                    if (line != '') line += ','
 
                    line += array[i][index];
                }
                str += line + '\r\n';
            }
            return str;
        }
  
printDiv(eleId) {
        var printContents = document.getElementById(eleId).outerHTML;
        var originalContents = document.body.innerHTML;    

    var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:0.3px solid #A3A2A2;' +
        'padding:0.5em;' +
        '}' +
        'table tr {' +
        'page-break-inside : avoid ;'+
        'page-break-after : avoid ;'+
        '}'+
        'table  {' +
        'page-break-inside:auto;'+
        'border : none ;'+
        
        '}'+
        '</style>';
    htmlToPrint += printContents;
    var newWin=window.open(); 
    newWin.document.write('<!DOCTYPE html><html><body><h1>API-Dev Portal</h1><br></br><div>'+ htmlToPrint +
    '</div></body></html>');
    newWin.print();
    newWin.onfocus=function(){ newWin.close();}
    return true;
  } 
       
 showInActiveStatus(isChecked){
       this.isInActiveCheckbox  = isChecked;
            if(!this.isInActiveCheckbox)
            {
                    this.viewAllUsers = this.viewAllUsers.filter(function(qt){
                        console.log(qt.status);
                        if ("Y" === qt.status){
                            return qt;
                        }                       
                    })    
            }
            else{
                this.viewAllUsers = this.tempAllUsers;
            }
       }

copyToClipboard(elementId) {
           var textarea = document.createElement("textarea");
           textarea.textContent = document.getElementById(elementId).innerText;
           textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
           document.body.appendChild(textarea);
           textarea.select()
           try {
               document.execCommand("copy");  // Security exception may be thrown by some browsers.
           } catch (ex) {
               console.warn("Copy to clipboard failed.", ex);
               return false;
           } finally {
               this.successMessage(true);
               document.body.removeChild(textarea);
           }
       }

       //Showing success message based on timer
successMessage(isvisible) {
           this.timer = Observable.interval(1000).take(3);
           this.timer.subscribe(t => this.tickerFunc(t));
           this.showModal = isvisible;
       }
tickerFunc(tick) {
           this.ticks = tick
           if (this.ticks >= 2) {
               this.showModal = false;
               this.ticks = 0;
           }
           return false;
       }
       sort(type,key){
         this.errorMsg = false;
        if(type == 'Asc')
        {
            this.sortVal = key;
            this.sortType = 'Desc';
        }else if(type == 'Desc')
        {
            this.sortVal = "-"+key;
            this.sortType = 'Asc';
        }
    }

generatePDF(compId) {
    console.log("1");
  var pdfsize = 'a4';
  var pdf = new jsPDF('P', 'pt', pdfsize);
  pdf.text(250, 40, "API-Dev Portal");
  var res = pdf.autoTableHtmlToJson(document.getElementById(compId));
  pdf.autoTable(res.columns, res.data, {
    startY: 60,
    styles: {
      overflow: 'linebreak',
      fontSize: 10,
      rowHeight: 20,
      columnWidth: 'auto',
      textColor: '#000000',
    },
    createdHeaderCell: function (cell, data) {
  if (cell.raw == 'STATUS' || cell.raw == 'PLATFORM' ||cell.raw == 'FIRST NAME' || cell.raw == 'LAST NAME'|| cell.raw == 'EMAIL ID' || cell.raw == 'USER ID' || cell.raw == 'ROLE'){//paint.Name header red
                       cell.styles.fontSize= 11;
                       cell.styles.textColor = '#ffffff';
                       cell.styles.fillColor =  [52, 73, 94];
                       cell.styles.rowHeight =  25
                    } else {
                        cell.styles.textColor = 255;
                        cell.styles.fontSize = 10;

                    }
                },
                columnStyles: {
                1: {columnWidth: 'auto'}
              }

                     });
                 pdf.save("API-Dev Portal.pdf");
                           }
}

 