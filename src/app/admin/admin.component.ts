import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.less']
})
export class AdminComponent implements OnInit {
pageTitle:string ;
errorMessage:string;
    public constructor(private titleService: Title) { 
         
            this.setTitle();
                 }
        public getTitle()
        {
            this.pageTitle = this.titleService.getTitle();
        }
         public setTitle() {
             this.getTitle();       
            this.titleService.setTitle( this.pageTitle +" Admin" );
    }

  ngOnInit(){
    }

}

