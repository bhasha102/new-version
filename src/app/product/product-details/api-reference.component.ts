import { Component, EventEmitter, Input, OnChanges, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { ProductDetailService } from './product-detail.service';

@Component({
    selector: 'api-reference-container',
    templateUrl: './api-reference.component.html',
    styleUrls : ['./product-detail.component.less', '../../../assets/axp-dev-portal/css/common.css']
})

export class ApiReferenceComponent implements OnInit {
    sub :any;
    documentId : number;
    documentName : string;
    documentOrgId : number;
    documentRoleId : number;
    isPublic : boolean;
    swaggerJson : JSON;

    constructor(private route: ActivatedRoute , private _productDetailService : ProductDetailService, private router:Router){
    }
    
    ngOnInit(){
         this.sub = this.route.params.subscribe(params => {
            this.documentId = +params['id'];            // + converts string to number
            this.documentName = params['name'];
            if(!this.isPublic){
                if(params['orgId'])
                {
                    this.documentOrgId = +params['orgId'];
                }
                if(params['roleId'])
                {
                    this.documentRoleId = +params['roleId'];
                }
            }
        });
        
        this._productDetailService.getSwaggerJSON().subscribe(
                swaggerFile => this.swaggerJson = swaggerFile,
                error =>  {console.log(error)},
                () => {
                        var apigeehosturl = "developers.intra.dev.aexp.com";
                        window['swaggerUi'] = new window['SwaggerUi']({
                        spec: this.swaggerJson,
                        dom_id: "swagger-ui-container",
                        supportedSubmitMethods: ['get', 'post', 'put', 'delete'],
                        useJQuery: true,
                        onComplete: function(swaggerApi, swaggerUi) {
                            /*if (typeof initOAuth == "function") {
                            
                                initOAuth({
                                clientId: "your-client-id",
                                realm: "your-realms",
                                appName: "your-app-name"
                                });
                                
                            }*/
                            window['jQuery']('pre code').each(function(i, e) {
                                window['hljs'].highlightBlock(e)
                            });
                        },
                        onFailure: function(data) {
                            console.log("Unable to Load SwaggerUI");
                        },
                        docExpansion: "none",
                        sorter: "alpha"
                    });

                     window['swaggerUi'].load(apigeehosturl);
                }
        );

        
    }


}