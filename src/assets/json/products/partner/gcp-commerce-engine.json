{
	"swagger": "2.0",
	"info": {
		"title": "GCP Commerce Engine",
		"description": "<p><strong>Accept American Express</strong> allows new and existing merchants to accept and approve payments using an American Express credit card.</p>",
		"version": "2.0.0",
		"x-modified": "July 28, 2016"
	},
	"x-basePaths": [
		{ "basePath": "/platform/v1/context/context"}
	],
	"x-security": {
		"oneWayTLS_AppKey": false,
		"oneWayTLS_HMAC": true,
		"oAuth": false
	},
	"x-samples": [
		{
			"name": "MAC creation library",
			"href": "https://github.com/americanexpress/common-libraries/MAC"
		},
		{
			"name": "Java Application",
			"href": "https://github.com/americanexpress/template/java"
		},
		{
			"name": "nodejs Sample",
			"href": "https://github.com/americanexpress/template/nodejs"
		},
		{
			"name": "some new link",
			"href": "https://github.com/americanexpress/template/nodejs"
		}
	],
	"schemes": [
		"https"
	],
	"consumes": [
		"application/json"
	],
	"paths": {
		"/authorizations": {
			"post": {
				"summary": "summary",
				"description": "<p>description</p>",
				"produces": [
					"application/json"
				],
				"parameters": [
					{
						"in": "header",
						"name": "x-amex-api-key",
						"description": "<p>Your application's Client Key for accessing this API.</p><p><em><a href='#/essentials#apisecurity'>Learn more about API Key security</a></em></p><p>e.g. <code>cOthV4ao4Xxho1BzrIXcOf8CTJXUONdj</code></p>",
						"required": true,
						"type": "string",
						"maxLength": 36
					},
					{
						"in": "header",
						"name": "authorization",
						"description": "<p>Authentication and Authorization token using HMAC SHA 256 algorithm.</p><p><em><a href='#/essentials#apisecurity'>Learn more about HMAC headers</a></em></p><p>e.g. <a href='#/essentials#apisecurity'>Please see the HMAC example here.</a></p>",
						"required": true,
						"type": "string",
						"maxLength": 36
					},
					{
						"in": "body",
						"name": "request",
						"schema": {
							"type": "object",
							"properties": {
								"CardTransaction": {
									"description": "<p>The root element that encloses all of the data elements that comprise this request’s record.</p>",
									"x-required": true,
									"properties": {
										"CardAcceptorDetail": {
											"type": "object",
											"description": "<p>Contains the Card Acceptor’s name and location details.</p>",
											"x-required": true,
											"properties": {
												"CardAcptNm": {
													"type": "string",
													"maxLength": 30,
													"description": "<p>The Card Acceptor’s (merchant's or seller’s) doing-business-as name.</p><p>This field is mandatory for payment service providers (aggregators) and optional-but-recommended for all other transactions.</p><p>Formats:</p><p><ul><li>Payment Service Providers (aggregators): unique, payment service provider (Aggregator) name and seller name, 37-byte (maximum), in the form of: Payment Service Provider (aggregator) = Seller DBA.</li></ul></p><p>e.g. <code>ANY AGGREGATOR=KATIS BEACH UMBRELLAS</code></p>",
													"x-required": false
												},
												"CardAcptStreetNm": {
													"type": "string",
													"maxLength": 29,
													"description": "<p>The street name of the Card Acceptor’s address.</p><p>This field is mandatory for payment service providers (aggregators) and optional-but-recommended for all other transactions.</p><p>e.g. <code>1234 ABC ST</code></p>",
													"x-required": false
												},
												"CardAcptCityNm": {
													"type": "string",
													"maxLength": 14,
													"description": "<p>The city name of the Card Acceptor’s address.</p><p>This field is mandatory for payment service providers (aggregators) and optional-but-recommended for all other transactions.</p><p>e.g. <code>ANYTOWN</code></p>",
													"x-required": false
												},
												"CardAcptPostCd": {
													"type": "string",
													"maxLength": 10,
													"description": "<p>The postal or zip code of the Card Acceptor’s address.</p><p>This field is mandatory for payment service providers (aggregators) and optional-but-recommended for all other transactions.</p><p>e.g. <code>12345-1234</code>, <code>A1B 2C3</code>, etc.</p>",
													"x-required": false
												},
												"CardAcptRgnCd": {
													"type": "string",
													"maxLength": 3,
													"description": "<p>The region code representing the region or state where the Card Acceptor is located.</p><p>This field is mandatory for payment service providers (aggregators) and optional for all other transactions.</p><p>For a list of region codes, refer to the <em>American Express Global Codes & Information Guide</em>.</p>",
													"x-required": false
												},
												"CardAcptCtryCd": {
													"type": "string",
													"maxLength": 3,
													"description": "<p>The country code representing the region or state where the Card Acceptor is located.</p><p>This field is mandatory for payment service providers (aggregators) and optional for all other transactions.</p><p>For a list of country codes, refer to the <em>American Express Global Codes & Information Guide.</em></p><p>e.g. <code>AZ</code></p>",
													"x-required": false
												}
											}
										}
									}
								}
							}
						}
					}
				],
				"responses": {
					"500": {
						"description": "Internal Error. An error has occured within the American Express systems.",
						"schema": {
							"type": "object",
							"properties": {
								"error_code": {
									"type": "string",
									"maxLength": 6,
									"description": "<p>Detailed Error Code.</p><p>Please see <a href='/essentials/error-codes'>Standard Error Codes</a> for additional details.</p><p>e.g. <code>104010</code></p>"
									},
								"error_type": {
									"type": "string",
									"description": "<p>Brief error description.</p><p>e.g. <code>InvalidApiKey</code></p>"
									},
								"error_description": {
									"type": "string",
									"description": "<p>Human-friendly detailed error description.</p><p>e.g. <code>Invalid API Key identified on the gateway</code></p>"
									},
								"more_information": {
									"type": "string",
									"description": "<p>Additional information related to the error. This could include low-level error codes and information.</p><p>e.g. <code>INQ2002 Card is not eligible for the Program</code></p>"
								}
							}
						}
					}
				},
				"x-tryit": [
					{
						"name": "placeholder",
						"requestHeaders": "GET /api",
						"requestBody": "",
						"responseHeaders": "HTTP/1.1 200 Sucess<br />Content-Type: application/json;",
						"responseBody": {"json": "info"},
						"responseCode": "200",
						"responseMessage": "Success"
					}
				]
			}
		}
	},
	"definitions": {},
	"x-testInfo": [
		{
			"categoryId": "cat1",
			"categoryName": "cat_name",
			"description": "description",
			"elements": [
			{
				"type": "blah",
				"usage": "blah",
				"value": "blah",
				"result": "blah"
			}
			]
		}
	]
}
