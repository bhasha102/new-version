# Design Language System by American Express


## Table of Contents
- [Introduction](#introduction)
- [Team](#team)
- [Requirements](#requirements)
- [Project Setup](#setup)
- [Building](#building)
- [Deployment](#deployment)
- [Environments](#environments)
- [Additional Resources](#resources)

## <a name="introduction"></a>Introduction
The American Express Design Language System.

## <a name="team"></a>Team
#### Producers
- **Executive Producer:** [Myke Gerstein](mailto:myke.gerstein@b-reel.com)
- **Producer:** [Ryan Leong](mailto:ryan.leong@b-reel.com)

#### Creative
- **Creative Director:** [Jed Grossman](mailto:jed.grossman@b-reel.com)
- **Art Director:** [Vincent Lowe](mailto:vincent.lowe@b-reel.com)
- **Designer:** [Eric Margusity](mailto:eric.margusity@b-reel.com)

#### Technical
- **Technical Director:** [Eric Heaton](mailto:eric.heaton@b-reel.com)
- **Lead Front-End Developer:** [Adam Hartwig](mailto:adam.hartwig@b-reel.com)
- **Front-End Developer:** [Dave Seidman](mailto:dave.seidman@b-reel.com)

## <a name="requirements"></a>Requirements
#### Development
NodeJS, Gulp, JSPM

## <a name="environments"></a>Environments

## <a name="setup"></a>Project Setup
1. Download and install [Node](https://nodejs.org)
2. Install local Node dependencies `npm install`
3. Install JSPM packages `jspm install` Update if needed: `jspm update`

## <a name="building"></a>Building
#### Production Build
TBA
#### Development Build
1. Run through the Project Setup to install the required dependencies.
2. From the root directory, run `gulp build` in the command line. `gulp build --debug` may be used to output source maps
3. `gulp watch [--debug]` is also available

## <a name="deployment"></a>Deployment
#### Development
TBA
#### Staging
TBA

## <a name="resources"></a>Additional Resources
[Node](https://nodejs.org)
[Jekyll](https://jekyllrb.com)
[JSPM](http://jspm.io)
