//
// Tooltip Model
//
// The model for a tooltip, each parameter may be controlled with
// data attributes on the selected element, or by specifying a Configuration
// manually when creating a tooltip.

import Model from '../../core/model';

export default class TooltipModel extends Model {}

TooltipModel.prototype.defaults = {
	force: false,
	manual: false,
	type: 'default',
	content: '',
	theme: 'default',
	classes: '',
	style: '',
	placement: 'top',
	size: 'rg',
	delay: 0.2,
	animation: 'anim-fade-tooltip',
	trigger: 'hover click', // accepts combinations of: 'hover click manual'
	constraints: [
		{
			to: 'scrollParent',
			attachment: 'together',
			pin: true
		}
	],
	offset: '0 0'
};
