//
// Slider Model
//

import Model from '../../core/model';

export default class SliderModel extends Model {}

SliderModel.prototype.defaults = {
	controls: false,
	stepInterval: 50,
	stepDelay: 500,
	min: 0,
	max: 10,
	step: 1,
	value: 5,
	tooltip: 'true',
	tooltipTheme: 'light',
	prefix:'',
	suffix:'',
	render: true
};
