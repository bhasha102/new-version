//
// Draggable Utility
//
// Makes a provided element become a draggable
// component. This class only acts as a unified event
// emitter; it does not handle the actual moving of
// any object.

import Model from '../core/model';

export default class Draggable extends Model {

	constructor (opts) {
		super({
			movedX: 0,
			movedY: 0,
			offsetX: 0,
			offsetY: 0,
		});

		this.set('uid', this.util.request('uid:gen'));

		this.animationFrame;
		this.target = opts.target;
		this.$scroll = opts.$scroll || $('body');

		if(!this.target)
			console.warn('[DLS > Util > Draggable]: Draggable elements require an $el to be specified.');
		this.configure();
		this.render();
	}

	//
	// Configure based on device
	//
	configure() {
		if(this.platform.touch)
			this.bindings = {
				down: this.onTouchStart
			};
		else
			this.bindings = {
				down: this.onMouseDown
			};
	}

	//
	// Render
	//
	render() {
		this.listen();
	}

	//
	// Bind the events
	//
	listen() {
		this.target
			.on(this.platform.event.down, e => {
				this.bindings.down(e, this);
				e.stopPropagation();
				e.preventDefault();
			});
	}

	//
	// Skip to position
	//
	skipToPosition(e) {
		this.handleDragStart(e, true);
	}

	//
	// On Drag start
	//
	handleDragStart(e, skip) {
		if(typeof e.button === 'number' && e.button !== 0)
			return;

		if(e.targetTouches)
			this.set({touchIdentifier: e.targetTouches[0].identifier});

		let {clientX, clientY, offsetX, offsetY} = this.getControlPosition(e);

		if(!this.get('dragging')) {
			this.target.addClass('dragging');
			this.emit('drag:start');
			$(this.$scroll)
				.on(this.platform.event.move+'.draggable', e => {
					this.handleDrag(e);
					e.stopPropagation();
					e.preventDefault();
				});

			$(document)
				.on(this.platform.event.up+`.draggable.${this.attributes.uid}`, e => {
					this.handleDragStop(e);
					e.stopPropagation();
					e.preventDefault();
				});
		}

		if(skip) {
			clientX = this.target.offset().left + (this.target.width() * 0.5);
			clientY = this.target.offset().top + (this.target.height() * 0.5);
		}

		this.set({
			dragging: true,
			startX: clientX - offsetX,
			startY: clientY - offsetY,
			offsetX: offsetX,
			offsetY: offsetY,
			lastX: clientX - offsetX,
			lastY:clientY - offsetY,
			movedX: 0,
			movedY: 0,
			scrollX: this.$scroll.scrollLeft,
			scrollY: this.$scroll.scrollTop
		});

		if(skip) {
			let {clientX, clientY, offsetX, offsetY} = this.getControlPosition(e);
			this.set({
				lastX: clientX - offsetX,
				lastY: clientY - offsetY,
				movedX: clientX-this.get('startX'),
				movedY: clientY-this.get('startY')
			});
		}
	}

	//
	// On Drag
	//
	handleDrag(e) {
		if(e.targetTouches && (e.targetTouches[0].identifier !== this.attributes.touchIdentifier))
			return;
		let {clientX, clientY, offsetX, offsetY} = this.getControlPosition(e);

		cancelAnimationFrame(this.animationFrame);
		this.animationFrame = requestAnimationFrame(e => {
			this.set({
				lastX: clientX - offsetX,
				lastY: clientY - offsetY,
				movedX: clientX-this.get('startX'),
				movedY: clientY-this.get('startY')
			});
		});
	}

	//
	// On Drag end
	//
	handleDragStop(e) {
		if(!this.get('dragging'))
			return;

		if(e.changedTouches && (e.changedTouches[0].identifier !== this.attributes.touchIdentifier))
			return;

		let {clientX, clientY, offsetX, offsetY} = this.getControlPosition(e);

		this.set({
			dragging: false,
			lastX: null,
			lastY: null,
			offsetX: 0,
			offsetY: 0
		});

		$(this.$scroll)
			.off(this.platform.event.move+'.draggable');

		$(document)
			.off(this.platform.event.up+`.draggable.${this.attributes.uid}`);

		this.target.removeClass('dragging');
		this.emit('drag:end');
	}

	//
	// Get control position
	//
	getControlPosition(e) {
		e = e.originalEvent;
		let position = (e.targetTouches && e.targetTouches[0]) || (e.changedTouches && e.changedTouches[0]) || e;

		return {
			clientX: position.clientX-this.attributes.offsetX,
			clientY: position.clientY-this.attributes.offsetY,
			offsetX: position.offsetX || 0,
			offsetY: position.offsetY || 0
		}
	}

	//
	// On Mouse Down
	//
	onMouseDown(e, _this) {
		return _this.handleDragStart(e);
	}

	//
	// On Touch Start
	//
	onTouchStart(e, _this) {
		return _this.handleDragStart(e);
	}

	//
	// Destroy (unused)
	//
	destroy() {}
}
