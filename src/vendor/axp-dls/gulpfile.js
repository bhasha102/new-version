//
// Gulp
//
// All tasks start here.

'use strict';

process.env.SERVE_TYPE = 'development';

var gulp = require('gulp'),
	config = require('./_gulp-config'),
	$ = require('gulp-load-plugins')(),
	cwd = './gulp_tasks/',
	tasks = require('fs').readdirSync(cwd);

tasks.forEach(function(task) {
	if(task.indexOf('.js')>-1)
		require(cwd+task)(gulp, config, $);
});
