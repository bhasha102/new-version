//
// temporary task to remove extranneous git tags from local and origin
//

'use strict';

module.exports = function(gulp, config, $) {

    var exec = require('sync-exec');

    // some old tags that exist locally and keep popping up:
    var tags = [
        '1.0.1',
        '1.0.2',
        '1.0.3',
        '1.0.5',
        '1.0.59-beta.5',
        '1.0.59-beta.6',
        '1.0.6',
        '1.0.7',
        '1.0.75',
        'undefined',
        'v1.0_Beta-1',
        'v1.0_Beta-2',
        'v1.0_Beta-3',
        'v1.0_Beta-4',
        'v1.0_Beta-5'
    ];


    gulp.task('cleantags', function() {

        for(var i = 0; i < tags.length; i++) {

            var tag = tags[i];
            console.log("Deleting Tag", tag);
            console.log(exec('git tag -d ' + tag));
            console.log(exec('git push origin :refs/tags/' + tag));
        }
    });

}
